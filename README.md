# RemoteCamera

Python library for communicating with wifi and bluetooth DSLRs and mirrorless cameras.

## Device enumeration

```python
from remotecamera import CameraLocator

locator = CameraLocator()

# This does threading magic to do background searches of wifi and bluetooth devices
for camera in locator.run_yield():
    print(camera)
    # camera = <Lumix {ip}>

```

## Device control

```python
from remotecamera.lumix import Lumix

# Device object comes from the locator or can be manually created:
camera = Lumix("192.168.2.42")

# Create the actual device connection and enumerate base info
camera.connect()
print(camera.model)  # GX7
print(camera.settings)

# Enable the flash and take a picture
camera.change_setting('flash', 'forcedflashon')
camera.capture()
```