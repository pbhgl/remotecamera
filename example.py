import time

from remotecamera.lumix import Lumix

camera = Lumix("192.168.2.41")
camera.connect()

camera.change_setting('flash', 'forcedflashon')
camera.capture()
time.sleep(1)

camera.change_setting('flash', 'forcedflashoff')
camera.capture()
