class Device:
    def __init__(self):
        pass

    def connect(self):
        """Run initial communications when you actually want to use this instance"""
        pass

    def update_state(self):
        """Refresh values that need polling"""
        pass

    def __eq__(self, other):
        if isinstance(other, Device):
            return self.__hash__() == other.__hash__()
        else:
            return False


class Setting:
    def __init__(self, name):
        self.name = name
        self.value = None
        self.options = {}
