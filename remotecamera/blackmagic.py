import struct

import dbus

from remotecamera.generic import Device

BMD_SERVICE = '291d567a-6d75-11e6-8b77-86f30ca893d3'


class BMDMessage:
    def __init__(self, device, command, payload):
        self.device = device
        self.command = command
        self.length = len(payload)
        self.payload = payload

    def get_bytes(self):
        padding = self.length % 4
        raw = struct.pack('<BBBB'.format(self.length, padding), self.device, self.length, self.command, 0)
        raw += self.payload
        raw += b'\0' * padding
        return raw


class BlackMagicCamera(Device):
    def __init__(self, bus, path, deviceinterface):
        super().__init__()

        self.bus = bus
        self.path = path
        self.deviceinterface = deviceinterface
        self.model = deviceinterface['Name']
        self.alias = deviceinterface['Alias']
        self.address = deviceinterface['Address']
        self.addressType = deviceinterface['AddressType']
        self.adapter = deviceinterface['Adapter']
        self.state = 'unknown'

        self.device = dbus.Interface(bus.get_object('org.bluez', path), 'org.bluez.Device1')

        self.service = None

        manager = dbus.Interface(bus.get_object('org.bluez', '/'), 'org.freedesktop.DBus.ObjectManager')
        servicepath = None
        for path, ifaces in manager.GetManagedObjects().items():
            if path.startswith(self.path):
                if 'org.bluez.GattService1' in ifaces:
                    if ifaces['org.bluez.GattService1']['UUID'] == BMD_SERVICE:
                        self.service = ifaces['org.bluez.GattService1']
                        servicepath = path
                        break
        else:
            raise ConnectionError("Unusable device")

        self.char_occ = None
        self.char_icc = None
        self.char_timecode = None
        self.char_status = None
        self.char_name = None
        self.char_version = None

        for path, ifaces in manager.GetManagedObjects().items():
            if path.startswith(servicepath + "/") and 'org.bluez.GattCharacteristic1' in ifaces:
                char = ifaces['org.bluez.GattCharacteristic1']
                if char['UUID'] == '8f1fd018-b508-456f-8f82-3d392bee2706':
                    self.char_version = dbus.Interface(bus.get_object('org.bluez', path),
                                                       'org.bluez.GattCharacteristic1')
                elif char['UUID'] == 'ffac0c52-c9fb-41a0-b063-cc76282eb89c':
                    self.char_name = dbus.Interface(bus.get_object('org.bluez', path),
                                                    'org.bluez.GattCharacteristic1')
                elif char['UUID'] == '7fe8691d-95dc-4fc5-8abd-ca74339b51b9':
                    self.char_status = dbus.Interface(bus.get_object('org.bluez', path),
                                                      'org.bluez.GattCharacteristic1')
                elif char['UUID'] == '6d8f2110-86f1-41bf-9afb-451d87e976c8':
                    self.char_timecode = dbus.Interface(bus.get_object('org.bluez', path),
                                                        'org.bluez.GattCharacteristic1')
                elif char['UUID'] == 'b864e140-76a0-416a-bf30-5876504537d9':
                    self.char_icc = dbus.Interface(bus.get_object('org.bluez', path),
                                                   'org.bluez.GattCharacteristic1')
                elif char['UUID'] == '5dd3465f-1aee-4299-8493-d2eca2f8e1bb':
                    self.char_occ = dbus.Interface(bus.get_object('org.bluez', path),
                                                   'org.bluez.GattCharacteristic1')

    def connect(self):
        self.device.Connect()

    def update_state(self):
        status = self.char_status.ReadValue({})
        sb = status[0]
        if sb == b'\0':
            self.state = 'unknown'
        elif sb == b'\x01':
            self.state = 'camera-power-on'
        elif sb == b'\x02':
            self.state = 'connected'
        elif sb == b'\x04':
            self.state = 'paired'
        elif sb == b'\x08':
            self.state = 'versions-verified'
        elif sb == b'\x10':
            self.state = 'initial-payload-received'
        elif sb == b'\x20':
            self.state = 'camera-ready'

    def change_config(self, category, parameter, datatype, data, relative=False):
        if relative:
            operation = 1
        else:
            operation = 0
        if datatype == 'void' or datatype == 'boolean':
            datatype = 0
        elif datatype == 'int8':
            datatype = 1
        elif datatype == 'int16':
            datatype = 2
        elif datatype == 'int32':
            datatype = 3
        elif datatype == 'int64':
            datatype = 4
        elif datatype == 'utf8':
            datatype = 5
        elif datatype == 'fixed16':
            datatype = 128

        payload = struct.pack('<BBBB', category, parameter, datatype, operation)
        payload += data
        msg = BMDMessage(255, 0, payload)
        self.char_occ.WriteValue(msg.get_bytes(), {})

    def set_gamma(self, red, green, blue, luma):
        value = self._fixed16(red) + self._fixed16(green) + self._fixed16(blue) + self._fixed16(luma)
        self.change_config(8, 1, 'fixed16', value)

    def __hash__(self):
        return hash(self.path)

    def __repr__(self):
        return f'<BlackMagicCamera {self.alias}>'