import queue

from remotecamera.bluetooth import BluetoothLocator
from remotecamera.ssdp import SSDPDiscover


class CameraLocator:
    def __init__(self):
        self._ssdp = SSDPDiscover()
        self._ssdp.daemon = True

        self._bt = BluetoothLocator()
        self._bt.daemon = True

        self._q = queue.Queue()
        self._run = False

        self.devices = set()

    def run_yield(self):
        self.run(self._queue_callback)
        self._run = True
        while self._run:
            device = self._q.get()
            if device not in self.devices:
                self.devices.add(device)
                yield device

    def run(self, callback):
        self._ssdp.callback = callback
        self._ssdp.start()
        self._bt.callback = callback
        self._bt.start()

    def stop(self):
        self._run = False
        self._ssdp.stop()
        self._bt.stop()

    def _queue_callback(self, device):
        self._q.put(device)


if __name__ == '__main__':
    test = CameraLocator()
    for item in test.run_yield():
        print(item)
