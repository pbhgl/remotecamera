import threading
import time

import dbus

from remotecamera.blackmagic import BlackMagicCamera, BMD_SERVICE


class BluetoothLocator(threading.Thread):
    def __init__(self):
        super().__init__()
        self.bus = dbus.SystemBus()
        self.callback = None

    def run(self):
        manager = dbus.Interface(self.bus.get_object('org.bluez', '/'), 'org.freedesktop.DBus.ObjectManager')

        while True:
            for path, ifaces in manager.GetManagedObjects().items():
                if 'org.bluez.Device1' in ifaces:
                    device = ifaces['org.bluez.Device1']
                    if BMD_SERVICE in device['UUIDs']:
                        try:
                            camera = BlackMagicCamera(self.bus, path, device)
                            self.callback(camera)
                        except ConnectionError:
                            pass
            time.sleep(5)

    def stop(self):
        pass
