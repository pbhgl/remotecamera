import socket
import time
import threading
from http.client import HTTPResponse
from io import BytesIO

from remotecamera.lumix import Lumix


class BufferSocket:
    def __init__(self, data):
        if not isinstance(data, bytes):
            data = data.encode()
        self.data = data

    def makefile(self, *args, **kwargs):
        return BytesIO(self.data)


class SSDPDiscover(threading.Thread):
    # 30 seconds for search_interval
    SEARCH_INTERVAL = 5
    BCAST_IP = '239.255.255.250'
    BCAST_PORT = 1900

    def __init__(self):
        threading.Thread.__init__(self)
        self.interrupted = False
        self.callback = None

    def run(self):
        self.keep_search()

    def stop(self):
        self.interrupted = True
        print("upnp client stop")

    def keep_search(self):
        '''
        run search function every SEARCH_INTERVAL
        '''
        try:
            while True:
                self.search()
                for x in range(self.SEARCH_INTERVAL):
                    time.sleep(1)
                    if self.interrupted:
                        return
        except Exception as e:
            print('Error in upnp client keep search %s', e)

    def search(self):
        # Find all SSDP services in the network
        SSDP_DISCOVER = ('M-SEARCH * HTTP/1.1\r\n' +
                         'HOST: 239.255.255.250:1900\r\n' +
                         'MAN: "ssdp:discover"\r\n' +
                         'MX: 1\r\n' +
                         'ST: ssdp:all\r\n' +
                         '\r\n')

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            sock.sendto(SSDP_DISCOVER.encode('ASCII'), (self.BCAST_IP, self.BCAST_PORT))
            sock.settimeout(3)
            while True:
                data, addr = sock.recvfrom(1024)
                # HTTP expects a TCP socket
                buffer = BufferSocket(data)
                response = HTTPResponse(buffer)
                response.begin()

                if response.status != 200:
                    continue

                server = response.getheader('SERVER')
                if 'Panasonic-UPnP-MW/1.0' in server:
                    self.callback(Lumix(addr[0]))
                    continue
        except:
            sock.close()


if __name__ == '__main__':
    client = SSDPDiscover()
    client.run()
