import urllib.request
import xml.etree.ElementTree as ET
from urllib.parse import urlencode

from remotecamera.generic import Setting, Device


class Lumix(Device):
    def __init__(self, ip):
        super().__init__()
        self.ip = ip

        self.model = None
        self.protocol_version = None
        self.capa_commands = set()
        self.capa_state = set()

        self.settings = {}
        self.battery_level = None
        self.remain_capacity = None
        self.status = None
        self.state = {}

    def __hash__(self):
        return hash(self.ip)

    def _raw(self, mode, data=None, ignore_error=False):
        query = {'mode': mode}
        if isinstance(data, dict):
            query.update(data)
        elif data is not None:
            query['value'] = str(data)
        query = urlencode(query)
        response = urllib.request.urlopen(f'http://{self.ip}/cam.cgi?{query}')
        if response.status != 200:
            raise ConnectionError(response.status)

        # Modified for Lumix DC-TZ91: moved the 2 lines below because ET.parse() failed for self._raw('accctrl', ...).
        # For info: opening
        # http://192.168.54.1/cam.cgi?mode=accctrl&type=req_acc&value=4D454930-0100-1000-8001-021900031D85&value2=remotecam
        # in a browser results in the following output (replaced hex digits with xxxxxx):
        # ok,TZ91-xxxxxx,remote,encrypted
        if ignore_error:
            return

        tree = ET.parse(response.fp)
        root = tree.getroot()

        for child in root:
            if child.tag == 'result':
                if child.text != 'ok':
                    raise ConnectionError(child.text)
                break
        return root

    def _get_capability(self):
        response = urllib.request.urlopen(f'http://{self.ip}/cam.cgi?mode=getinfo&type=capability')
        if response.status != 200:
            raise ConnectionError(response.status)

        tree = ET.parse(response.fp)
        root = tree.getroot()

        for child in root:
            if child.tag == 'result':
                if child.text != 'ok':
                    raise ConnectionError(child.text)
            elif child.tag == 'comm_proto_ver':
                self.protocol_version = child.text
            elif child.tag == 'productinfo':
                for c in child:
                    self.model = c.text
            elif child.tag == 'camcmdlist':
                for c in child:
                    self.capa_commands.add(c.text)
            elif child.tag == 'settinglist':
                for settingroot in child:
                    name = settingroot.tag
                    setting = Setting(name)
                    for sc in settingroot:
                        if sc.tag == 'curvalue':
                            setting.value = sc.text
                        elif sc.tag == 'valuelist':
                            for option in sc.text.split(','):
                                setting.options[option] = option
                    self.settings[name] = setting
            elif child.tag == 'getstatelist':
                for c in child:
                    self.capa_state.add(c.text)

    def _pair(self):
        clientname = 'remotecam'
        udn = '4D454930-0100-1000-8001-021900031D85'
        self._raw('accctrl', {'type': 'req_acc', 'value': udn, 'value2': clientname}, ignore_error=True)
        self._raw('setsetting', {'type': 'device_name', 'value': clientname}, ignore_error=True)

    def connect(self):
        # Complete pairing to avoid the wifi connection timing out
        self._pair()

        # Query capabilities
        self._get_capability()

    def update_state(self):
        response = urllib.request.urlopen(f'http://{self.ip}/cam.cgi?mode=getstate')
        if response.status != 200:
            raise ConnectionError(response.status)

        tree = ET.parse(response.fp)
        root = tree.getroot()

        for child in root:
            if child.tag == 'result':
                if child.text != 'ok':
                    raise ConnectionError(child.text)
            elif child.tag == 'state':
                for state in child:
                    if state.tag == 'batt':
                        self.battery_level = state.text
                    elif state.tag == 'remaincapacity':
                        self.remain_capacity = int(state.text)
                    elif state.tag == 'sdcardstatus':
                        self.status = state.text
                    else:
                        self.state[state.tag] = state.text
                self.protocol_version = child.text

    def command(self, cmd):
        response = urllib.request.urlopen(f'http://{self.ip}/cam.cgi?mode=camcmd&value={cmd}')
        if response.status != 200:
            raise ConnectionError(response.status)

        tree = ET.parse(response.fp)
        root = tree.getroot()

        for child in root:
            if child.tag == 'result':
                if child.text != 'ok':
                    raise ConnectionError(child.text)

    def capture(self):
        # Modified for Lumix DC-TZ91: commented the 2 lines below.
        # For info: print(self.capa_commands) results in
        # {'4kphoto_marking', 'wide-fast', 'wide-normal', 'tele-fast', 'tele-normal'}
        # but self.command('capture') works

        #if 'capture' not in self.capa_commands:
        #    raise ValueError("Capture is not supported on this device")

        self.command('capture')

    def change_setting(self, setting, value):
        if isinstance(setting, Setting):
            if value not in setting.options:
                raise ValueError("Unsuppored value")
            setting = setting.name

        response = urllib.request.urlopen(f'http://{self.ip}/cam.cgi?mode=setsetting&type={setting}&value={value}')
        if response.status != 200:
            raise ConnectionError(response.status)

        tree = ET.parse(response.fp)
        root = tree.getroot()

        for child in root:
            if child.tag == 'result':
                if child.text == 'err_reject':
                    raise ValueError(f"Invalid value '{value}' for setting '{setting}'")
                if child.text != 'ok':
                    raise ConnectionError(child.text)

    def __repr__(self):
        return f'<Lumix {self.ip}: {self.model}>'
